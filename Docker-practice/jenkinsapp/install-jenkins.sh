#!/bin/bash

apt-get update && apt-get install -y tzdata && apt-get clean

apt-get install git wget unzip curl tree -y 

apt-get install openjdk-11-jdk -y

cp -pvr /etc/environment "/etc/environment_$(date +%F_%R)"

echo "JAVA_HOME=/usr/lib/jvm/java-11-openjdk-amd64/" | tee -a /etc/environment

source /etc/environment

cd /opt/

wget -q -O - https://pkg.jenkins.io/debian-stable/jenkins.io.key | apt-key add -

sh -c 'echo "deb https://pkg.jenkins.io/debian-stable binary/" > /etc/apt/sources.list.d/jenkins.list'

apt-get update 

apt-get install jenkins -y