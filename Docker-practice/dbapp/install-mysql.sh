#!/bin/bash

# Update Ubuntu Repository 
apt-get update && apt-get install -y tzdata && apt-get clean

# Download, & Install Utility Softwares 
# apt-get install git wget unzip curl tree -y 

# Download, Install & Configure Web Server i.e. Apache2 
apt-get install mysql-server mysql-client -y 

