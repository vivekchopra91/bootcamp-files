#!/bin/bash

# Create Docker volumes to store the SonarQube persistent data.
docker volume create sonarqube-conf 
docker volume create sonarqube-data
docker volume create sonarqube-logs
docker volume create sonarqube-extensions

# Optionally, create symbolic links to an easier access location.

sudo mkdir /sonarqube

sudo ln -s /var/lib/docker/volumes/sonarqube-conf/_data /sonarqube/conf
sudo ln -s /var/lib/docker/volumes/sonarqube-data/_data /sonarqube/data
sudo ln -s /var/lib/docker/volumes/sonarqube-logs/_data /sonarqube/logs
sudo ln -s /var/lib/docker/volumes/sonarqube-extensions/_data /sonarqube/extensions

# Start a SonarQube container with persistent data storage.
docker run -d --name sonarqube -P -v sonarqube-conf:/opt/sonarqube/conf -v sonarqube-data:/opt/sonarqube/data -v sonarqube-logs:/opt/sonarqube/logs -v sonarqube-extensions:/opt/sonarqube/extensions sonarqube
