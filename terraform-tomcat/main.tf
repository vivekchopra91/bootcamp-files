provider "aws" {
    region = "us-east-1"
    profile = "vd"
}

resource "aws_instance" "name" {
    ami = "ami-04505e74c0741db8d"
    instance_type = "t2.micro"
    key_name = "2nd-key"
    subnet_id = "subnet-036d81e3d88d4fa7b"
    vpc_security_group_ids = ["sg-012fa4f5511a5b66f"]
    user_data = "${file("install-tomcat.sh")}"

    tags = {
        "Name" = "Terraform-TomcatServer"
            Enviornment = "Dev"
            ProjectName = "Softobiz"
            ProjectID = "20220125"
            EmailID = "devops@softobiz.com"
            CreatedBy = "IAC-Terraform"
    }

}