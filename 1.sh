#!/bin/bash

# Update the Ubuntu Local Repository with Online Repository 
sudo apt-get update
sudo apt-get upgrade
sudo apt-get install software-properties-common -y && sudo apt-get clean

# Download, Install & Configure Utility Softwares 
sudo apt-get install git curl unizp tree wget -y

# Install Containerization Technology 
sudo apt-get install docker.io -y

sudo usermod -aG docker $USER

# Create a File and Update 
sudo touch /etc/docker/daemon.json

# docker change cgroup driver to systemd
echo '{'  >> /etc/docker/daemon.json
echo '"exec-opts": ["native.cgroupdriver=systemd"]'  >> /etc/docker/daemon.json
echo '}'  >> /etc/docker/daemon.json

# Enable Docker Services at boot level
sudo systemctl enable docker
sudo systemctl restart docker

# Update the apt package index and install packages needed to use the Kubernetes apt repository:
sudo apt-get update && apt-get clean
sudo apt-get install -y apt-transport-https ca-certificates

# Download the Google Cloud public signing key:
sudo curl -fsSLo /usr/share/keyrings/kubernetes-archive-keyring.gpg https://packages.cloud.google.com/apt/doc/apt-key.gpg

# Add the Kubernetes apt repository:
sudo echo "deb [signed-by=/usr/share/keyrings/kubernetes-archive-keyring.gpg] https://apt.kubernetes.io/ kubernetes-xenial main" | sudo tee /etc/apt/sources.list.d/kubernetes.list

# Update apt package index, install kubelet, kubeadm and kubectl, and pin their version:
sudo apt-get update && apt-get clean
sudo apt-get install -y kubelet kubeadm kubectl
sudo apt-mark hold kubelet kubeadm kubectl 

# kubeadm init --pod-network-cidr=10.244.0.0/16
