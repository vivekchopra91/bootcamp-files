#!/bin/bash

# Create a Variable
STATUS="$(sudo systemctl is-active docker.service)"

# Conditional Statements
if [ "$STATUS" == "inactive" ]; 
then
echo "starting docker service"
sudo systemctl restart docker.service
else
echo "docker is currently running"
fi
