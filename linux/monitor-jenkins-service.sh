#!/bin/bash

# Create a Variable
STATUS="$(sudo systemctl is-active jenkins.service)"

# Conditional Statements
if [ "$STATUS" == "inactive" ]; 
then
echo "starting jenkins service"
sudo systemctl restart jenkins.service
else
echo "jenkins is currently running"
fi
