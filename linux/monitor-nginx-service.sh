#!/bin/bash

# Create a Variable
STATUS="$(sudo systemctl is-active nginx.service)"

# Conditional Statements
if [ "$STATUS" == "inactive" ]; 
then
echo "starting nginx service"
sudo systemctl restart nginx.service
else
echo "nginx is currently running"
fi
