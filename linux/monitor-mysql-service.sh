#!/bin/bash

# Create a Variable
STATUS="$(sudo systemctl is-active mysql.service)"

# Conditional Statements
if [ "$STATUS" == "inactive" ]; 
then
echo "starting mysql service"
sudo systemctl restart mysql.service
else
echo "mysql is currently running"
fi
