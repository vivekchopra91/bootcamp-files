#!/bin/bash

# Create a Variable
STATUS="$(sudo systemctl is-active apache2.service)"

# Conditional Statements
if [ "$STATUS" == "inactive" ]; 
then
echo "starting apache2 service"
sudo systemctl restart apache2.service

STATUS1="$(sudo systemctl is-active apache2.service)"
# Conditional Statements
elif [ "$STATUS1" == "inactive" ]; 
then
echo "apache2 is not active, please check journalctl."

else
echo "apache2 is currently running"
fi