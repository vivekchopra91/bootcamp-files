FROM docker

RUN yum update -y

# Create a File and Update 
RUN touch /etc/docker/daemon.json

# docker change cgroup driver to systemd
RUN echo '{'  >> /etc/docker/daemon.json
RUN echo '"exec-opts": ["native.cgroupdriver=systemd"]'  >> /etc/docker/daemon.json
RUN echo '}'  >> /etc/docker/daemon.json

# Update the apt package index and install packages needed to use the Kubernetes apt repository:
RUN yum update -y
RUN yum install -y apt-transport-https ca-certificates

# Download the Google Cloud public signing key:
RUN curl -fsSLo /usr/share/keyrings/kubernetes-archive-keyring.gpg https://packages.cloud.google.com/apt/doc/apt-key.gpg

# Add the Kubernetes apt repository:
RUN echo "deb [signed-by=/usr/share/keyrings/kubernetes-archive-keyring.gpg] https://apt.kubernetes.io/ kubernetes-xenial main" | tee /etc/apt/sources.list.d/kubernetes.list

# Update apt package index, install kubelet, kubeadm and kubectl, and pin their version:
RUN yum update
RUN yum install -y kubelet kubeadm kubectl
RUN apt-mark hold kubelet kubeadm kubectl