#!/bin/bash

# Update the Ubuntu Local Repository with Online Repository 
sudo apt-get update && apt-get install software-properties-common -y && apt-get clean

# Download, Install & Configure Utility Softwares 
sudo apt-get install git curl unizp tree wget -y && apt-get clean

# Install Containerization Technology 
sudo apt-get install docker.io -y && apt-get clean

sudo usermod -aG docker $USER
sudo systemctl enable docker
sudo systemctl start docker
# Create a File and Update 
sudo touch /etc/docker/daemon.json

# docker change cgroup driver to systemd
echo '{'  >> /etc/docker/daemon.json
echo '"exec-opts": ["native.cgroupdriver=systemd"]'  >> /etc/docker/daemon.json
echo '}'  >> /etc/docker/daemon.json

# Update the apt package index and install packages needed to use the Kubernetes apt repository:
sudo apt-get update && apt-get clean
sudo apt-get install -y apt-transport-https ca-certificates

# Download the Google Cloud public signing key:
sudo curl -fsSLo /usr/share/keyrings/kubernetes-archive-keyring.gpg https://packages.cloud.google.com/apt/doc/apt-key.gpg

# Add the Kubernetes apt repository:
sudo echo "deb [signed-by=/usr/share/keyrings/kubernetes-archive-keyring.gpg] https://apt.kubernetes.io/ kubernetes-xenial main" | sudo tee /etc/apt/sources.list.d/kubernetes.list

# Update apt package index, install kubelet, kubeadm and kubectl, and pin their version:
sudo apt-get update && apt-get clean
sudo apt-get install -y kubelet kubeadm kubectl
sudo apt-mark hold kubelet kubeadm kubectl

sudo systemctl restart docker
sudo systemctl restart kubelet


# kubeadm init --pod-network-cidr=10.244.0.0/16
# swapoff -a
# kubectl get nodes --> check the nodes on controller machine
# 
# To start using your cluster, you need to run the following as a regular user:
# 
#   mkdir -p $HOME/.kube
#   sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
#   sudo chown $(id -u):$(id -g) $HOME/.kube/config
# 
# Alternatively, if you are the root user, you can run:
# 
#   export KUBECONFIG=/etc/kubernetes/admin.conf
# 
# You should now deploy a pod network to the cluster.
# Run "kubectl apply -f [podnetwork].yaml" with one of the options listed at:
#   https://kubernetes.io/docs/concepts/cluster-administration/addons/
# APPLY THIS
#   :--> kubectl apply -f https://raw.githubusercontent.com/flannel-io/flannel/master/Documentation/kube-flannel.yml
# 
# Then you can join any number of worker nodes by running the following on each as root:
# 
# kubeadm join 172.31.40.192:6443 --token wz61e0.ek8xkrte4tj9h7ox \
# 	--discovery-token-ca-cert-hash sha256:58cbe137600ac11bb61fad3af58e92018cc13154446b5a1a63405340664f99bd 
# 