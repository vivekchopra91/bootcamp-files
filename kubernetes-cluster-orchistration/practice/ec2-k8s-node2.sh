#!/bin/bash

# Create a Linux Operating System using AWS CLI Commands
aws ec2 run-instances \
--image-id "ami-04505e74c0741db8d" \
--instance-type "t2.medium" \
--count 1 \
--subnet-id "subnet-036d81e3d88d4fa7b" \
--tag-specifications 'ResourceType=instance,Tags=[{Key=Name,Value=k8s-Node2}]'  \
--security-group-ids  "sg-0a782eafbc9b08b7f"   \
--key-name "2nd-key" \
--user-data file://k8s-node2-install.txt --profile vd

# ,{Key=Environment,Value=Development},{Key=Project Name,Value=Hatigen},{Key=Project ID,Value=20211204},{Key=Email ID,Value=trainings@hatigen.com},{Key=Mobile Number,Value=+91}