# Authentication to AWS from Terraform code 
provider "aws" {
    region = "us-east-1"
    profile = "vd"
}

# Create a VPC in AWS part of region i.e. N.Virginia 
resource "aws_vpc" "bootcamp_vpc" {
    cidr_block = "10.0.0.0/16"
    instance_tenancy = "default"
    enable_dns_support = true
    enable_dns_hostnames = true 

    tags = {
        Name = "bootcamp_vpc"
        Created_By = "Terraform"
    }
}

# Create a Public-Subnet1 part of bootcamp_vpc 
resource "aws_subnet" "bootcamp_public_subnet1" {
    vpc_id = "${aws_vpc.bootcamp_vpc.id}"     # 0.11.7 
    cidr_block = "10.0.1.0/24"
    map_public_ip_on_launch = true 
    availability_zone = "us-east-1a"

    tags = {
        Name = "bootcamp_public_subnet1"
        created_by = "Terraform"
    }
}
resource "aws_subnet" "bootcamp_public_subnet2" {
    vpc_id = "${aws_vpc.bootcamp_vpc.id}"     # 0.11.7 
    cidr_block = "10.0.2.0/24"
    map_public_ip_on_launch = true 
    availability_zone = "us-east-1b"

    tags = {
        Name = "bootcamp_public_subnet2"
        created_by = "Terraform"
    }
}

resource "aws_subnet" "bootcamp_private_subnet1" {
    vpc_id = "${aws_vpc.bootcamp_vpc.id}"     # 0.11.7 
    cidr_block = "10.0.3.0/24"
    availability_zone = "us-east-1a"

    tags = {
        Name = "bootcamp_private_subnet1"
        created_by = "Terraform"
    }
}
resource "aws_subnet" "bootcamp_private_subnet2" {
    vpc_id = "${aws_vpc.bootcamp_vpc.id}"     # 0.11.7 
    cidr_block = "10.0.4.0/24"
    availability_zone = "us-east-1b"

    tags = {
        Name = "bootcamp_private_subnet2"
        created_by = "Terraform"
    }
}

# IGW
resource "aws_internet_gateway" "bootcamp_igw" {
    vpc_id = "${aws_vpc.bootcamp_vpc.id}"

    tags = {
        Name = "bootcamp_igw"
        Created_By = "Terraform"
    }  
}

# RTB
resource "aws_route_table" "bootcamp_rtb_public" {
    vpc_id = "${aws_vpc.bootcamp_vpc.id}"

    tags = {
        Name = "bootcamp_rtb_public"
        Created_By = "Teerraform"
    }
}
resource "aws_route_table" "bootcamp_rtb_private" {
    vpc_id = "${aws_vpc.bootcamp_vpc.id}"

    tags = {
        Name = "bootcamp_rtb_private"
        Created_By = "Teerraform"
    }
}

# Create the internet Access 
resource "aws_route" "bootcamp_rtb_igw" {
    route_table_id = "${aws_route_table.bootcamp_rtb_public.id}"
    destination_cidr_block = "0.0.0.0/0"
    gateway_id = "${aws_internet_gateway.bootcamp_igw.id}"

}

resource "aws_route_table_association" "bootcamp_subnet_association1" {
    subnet_id = "${aws_subnet.bootcamp_public_subnet1.id}"
    route_table_id = "${aws_route_table.bootcamp_rtb_public.id}"
}
resource "aws_route_table_association" "bootcamp_subnet_association2" {
    subnet_id = "${aws_subnet.bootcamp_public_subnet2.id}"
    route_table_id = "${aws_route_table.bootcamp_rtb_public.id}"
}
resource "aws_route_table_association" "bootcamp_subnet_association3" {
    subnet_id = "${aws_subnet.bootcamp_private_subnet1.id}"
    route_table_id = "${aws_route_table.bootcamp_rtb_private.id}"
}
resource "aws_route_table_association" "bootcamp_subnet_association4" {
    subnet_id = "${aws_subnet.bootcamp_private_subnet2.id}"
    route_table_id = "${aws_route_table.bootcamp_rtb_private.id}"
}

# Elastic Ipaddress for NAT Gateway
resource "aws_eip" "bootcamp_eip" {
  vpc = true
}

# Create Nat Gateway 
resource "aws_nat_gateway" "bootcamp_gw" {
    allocation_id = "${aws_eip.bootcamp_eip.id}"
    subnet_id = "${aws_subnet.bootcamp_public_subnet1.id}"

    tags = {
        Name = "Nat Gateway"
        Createdby = "Terraform"
    }
}

# Allow internet access from NAT Gateway to Private Route Table
resource "aws_route" "bootcamp_rtb_private_gw" {
    route_table_id = "${aws_route_table.bootcamp_rtb_private.id}"
    destination_cidr_block = "0.0.0.0/0"
    gateway_id = "${aws_nat_gateway.bootcamp_gw.id}"
}

# Network Access Control List 
resource "aws_default_network_acl" "bootcamp_nsg" {
    vpc_id = "${aws_vpc.bootcamp_vpc.id}"
    subnet_ids = [
    "${aws_subnet.bootcamp_public_subnet1.id}",
    "${aws_subnet.bootcamp_public_subnet2.id}",
    "${aws_subnet.bootcamp_private_subnet1.id}",
    "${aws_subnet.bootcamp_private_subnet2.id}"
    ]

    # Allow ingress / inound of all ports 
    ingress {
    protocol   = -1
    rule_no    = 100
    action     = "allow"
    cidr_block = aws_vpc.bootcamp_vpc.cidr_block
    from_port  = 0
    to_port    = 0
    }
    # Allow egress / outbound of all ports
    egress {
        protocol   = -1
        rule_no    = 100
        action     = "allow"
        cidr_block = "0.0.0.0/0"
        from_port  = 0
        to_port    = 0
    }
    tags = {
        Name = "bootcamp_nsg"
        createdby = "Terraform"
    }
}

# EC2 instance Security group - Bastion
resource "aws_security_group" "bootcamp_sg_bastion" {
    vpc_id = "${aws_vpc.bootcamp_vpc.id}"
    name   = "sg_bootcamp_ssh_rdp"
    description = "To Allow SSH From IPV4 Devices"

    # Allow ingress / inound of all ports 
    ingress {
        from_port = 0
        to_port   = 0
        protocol  = "-1"
        cidr_blocks = ["0.0.0.0/0"]
    }
    # Allow egress / outbound of all ports 
    egress {
        from_port = 0
        to_port   = 0
        protocol  = "-1"
        cidr_blocks = ["0.0.0.0/0"]
    }

    tags = {
        Name = "bootcamp_sg_bastion"
        Description = "bootcamp allow SSH - RDP"
        createdby = "terraform"
    }

}

# EC2 instance Security group - WebServer
resource "aws_security_group" "bootcamp_sg_web" {
    vpc_id = "${aws_vpc.bootcamp_vpc.id}"
    name   = "sg_bootcamp_ssh"
    description = "To Allow SSH From IPV4 Devices"

    # Allow ingress / inound of all ports 
    ingress {
        from_port = 0
        to_port   = 0
        protocol  = "-1"
        cidr_blocks = ["0.0.0.0/0"]
    }
    # Allow egress / outbound of all ports 
    egress {
        from_port = 0
        to_port   = 0
        protocol  = "-1"
        cidr_blocks = ["0.0.0.0/0"]
    }

    tags = {
        Name = "bootcamp_sg_web"
        Description = "bootcamp allow SSH - HTTP and Jenkins"
        createdby = "terraform"
    }
}

# Bastion - Windows 
resource "aws_instance" "bootcamp_bastion" {
    ami = "ami-00cefd54ba36fdf42"
    instance_type = "t2.micro"
    key_name = "2nd-key"
    subnet_id = "${aws_subnet.bootcamp_public_subnet1.id}"
    vpc_security_group_ids = ["${aws_security_group.bootcamp_sg_bastion.id}"]
    tags = {
        Name = "bootcamp_bastion"
        CreatedBy = "Terraform"
    }
}

# WebServer - Linux
resource "aws_instance" "bootcamp_webserver" {
    ami = "ami-04505e74c0741db8d"
    instance_type = "t2.micro"
    key_name = "2nd-key"
    subnet_id = "${aws_subnet.bootcamp_public_subnet1.id}"
    vpc_security_group_ids = ["${aws_security_group.bootcamp_sg_web.id}"]
    user_data = "${file("web.sh")}"

    tags = {
        "Name" = "Terraform-ApacheWebServer"
            Enviornment = "Dev"
            ProjectName = "Softobiz"
            ProjectID = "20220127"
            EmailID = "devops@softobiz.com"
            CreatedBy = "Terraform"
    }
}

# DBServer - Linux
resource "aws_instance" "bootcamp_dbserver" {
    ami = "ami-04505e74c0741db8d"
    instance_type = "t2.micro"
    key_name = "2nd-key"
    subnet_id = "${aws_subnet.bootcamp_public_subnet1.id}"
    vpc_security_group_ids = ["${aws_security_group.bootcamp_sg_web.id}"]
    user_data = "${file("db.sh")}"

    tags = {
        "Name" = "Terraform-DBServer"
            Enviornment = "Dev"
            ProjectName = "Softobiz"
            ProjectID = "20220125"
            EmailID = "devops@softobiz.com"
            CreatedBy = "Terraform"
    }
}

# TomcatServer - Linux
resource "aws_instance" "bootcamp_tomcatserver" {
    ami = "ami-04505e74c0741db8d"
    instance_type = "t2.micro"
    key_name = "2nd-key"
    subnet_id = "${aws_subnet.bootcamp_public_subnet1.id}"
    vpc_security_group_ids = ["${aws_security_group.bootcamp_sg_web.id}"]
    user_data = "${file("install-tomcat.sh")}"

    tags = {
        "Name" = "Terraform-TomcatServer"
            Enviornment = "Dev"
            ProjectName = "Softobiz"
            ProjectID = "20220125"
            EmailID = "devops@softobiz.com"
            CreatedBy = "Terraform"
    }
}