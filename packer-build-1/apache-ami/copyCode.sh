#!/bin/bash

cd /var/www/html/

git clone https://github.com/keshavkummari/keshavkummari.git

sudo systemctl stop httpd.service
sudo systemctl restart httpd.service
sudo systemctl reload httpd.service
