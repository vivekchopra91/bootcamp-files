#!/bin/bash

# Common Software Properties 
sudo apt install software-properties-common -y 

# cd /opt/

# add the repository key and file to Ubuntu.
wget -qO - https://api.bintray.com/orgs/jfrog/keys/gpg/public.key | apt-key add -

# Add Jfrog URL
sudo add-apt-repository "deb [arch=amd64] https://jfrog.bintray.com/artifactory-debs $(lsb_release -cs) main"

# Update the Repository on Ubuntu 18.04
sudo apt-get update 

# Install Jfrog 
sudo apt install jfrog-artifactory-oss -y 

# Start Jfrog Service
# sudo systemctl status artifactory.service
# sudo systemctl stop artifactory.service
# sudo systemctl start artifactory.service
# sudo systemctl restart artifactory.service
# sudo systemctl enable artifactory.service

# Open Browser and Validate 

# http://65.0.124.225:8081

# UserName : admin ;  Password : password 