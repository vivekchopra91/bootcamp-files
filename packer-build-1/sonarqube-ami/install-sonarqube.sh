#!/bin/bash

# Update Ubuntu Operating System Repository
sudo apt-get update

# Install Docker on Ubuntu Server 
sudo apt-get install docker.io -y 

# Download a Docker Image of Sonarqube 
sudo docker pull sonarqube

# check Downloaded Docker Image of Sonarqube
sudo docker images

# Create Docker volumes to store the SonarQube persistent data.
sudo docker volume create sonarqube-conf 
sudo docker volume create sonarqube-data
sudo docker volume create sonarqube-logs
sudo docker volume create sonarqube-extensions

# Verify the persistent data directories.
sudo docker volume inspect sonarqube-conf 
sudo docker volume inspect sonarqube-data
sudo docker volume inspect sonarqube-logs
sudo docker volume inspect sonarqube-extensions

# Optionally, create symbolic links to an easier access location.

sudo mkdir /sonarqube

sudo ln -s /var/lib/docker/volumes/sonarqube-conf/_data /sonarqube/conf
sudo ln -s /var/lib/docker/volumes/sonarqube-data/_data /sonarqube/data
sudo ln -s /var/lib/docker/volumes/sonarqube-logs/_data /sonarqube/logs
sudo ln -s /var/lib/docker/volumes/sonarqube-extensions/_data /sonarqube/extensions

# Start a SonarQube container with persistent data storage.
sudo docker run -d --name sonarqube -p 9000:9000 -p 9092:9092 -v sonarqube-conf:/opt/sonarqube/conf -v sonarqube-data:/opt/sonarqube/data -v sonarqube-logs:/opt/sonarqube/logs -v sonarqube-extensions:/opt/sonarqube/extensions sonarqube
