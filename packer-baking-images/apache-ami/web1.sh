#!/bin/bash

# # Update the Repository
# sudo apt-get update
# sudo apt-get install -f
# sudo dpkg --configure -a
# sudo apt-get clean
# sudo apt-get update
# sudo apt-get upgrade -y

# # Download, Install & Configure Ansible
# sudo apt-get install software-properties-common -y 

# sudo apt-get install git curl wget -y

# sudo apt-get install apache2 -y

# sudo systemctl start apache2

# Setup Hostname 
sudo hostnamectl set-hostname "web.softobiz.com"

cd /opt/

# Deploy a simple Website from Github 
sudo git clone https://gitlab.com/vivekchopra91/web-code.git

# Go inside of the folder
cd /web-code/
pwd
ls -lrta /var/www/
sudo chown -R $USER:$USER /opt/*
sudo chmod -R +rw /opt/*

# And move the code o document root
sudo mv * /var/www/html/

sudo systemctl status apache2
sudo systemctl restart apache2
sudo systemctl reload apache2