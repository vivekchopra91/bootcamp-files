#!/bin/bash

# Update the Repository
sudo apt-get update

# Download, Install & Configure Ansible
sudo apt-get install software-properties-common -y 

sudo apt-get install git curl wget -y

#sudo add-apt-repository --yes --update ppa:ansible/ansible

#sudo apt-get install ansible -y 

sudo apt-get install apache2 -y 

# Refresh the Terminal
/bin/bash

# Setup Hostname 
sudo hostnamectl set-hostname "web.softobiz.com"

# Update the hostname part of Host File
sudo /bin/sh -c 'echo "`hostname -I | awk '{ print $1 }'` `hostname`" >> /etc/hosts' 

cd /opt/

# Deploy a simple Website from Github 
sudo git clone https://gitlab.com/vivekchopra91/web-code.git

# Go inside of the folder
cd /web-code/
sudo chown -R $(whoami):$(whoami) /opt/*
sudo chmod -R +rwx /opt/*

# And move the code o document root
sudo mv * /var/www/html/
