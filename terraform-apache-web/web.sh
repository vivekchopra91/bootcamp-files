#!/bin/bash

# Setup Hostname 
sudo hostnamectl set-hostname "web.apache.softobiz.com"

# Update the hostname part of Host File
echo "`hostname -I | awk '{ print $1 }'` `hostname`" >> /etc/hosts 

# Update Ubuntu Repository 
apt update 

# Download, & Install Utility Softwares 
apt install git wget unzip curl tree -y 

# Download, Install & Configure Web Server i.e. Apache2 
apt install apache2 -y

cd /opt/

git clone https://github.com/Alphanum404/personal-web.git

cd /personal-web/

mv * /var/www/html/