#!/bin/bash

sudo apt-get install git wget unzip curl tree -y 

sudo apt-get install openjdk-11-jdk -y

sudo cp -pvr /etc/environment "/etc/environment_$(date +%F_%R)"

echo "JAVA_HOME=/usr/lib/jvm/java-11-openjdk-amd64/" >> /etc/environment

source /etc/environment

cd /opt/

sudo wget -q -O - https://pkg.jenkins.io/debian-stable/jenkins.io.key | sudo apt-key add -

sudo sh -c 'echo deb https://pkg.jenkins.io/debian-stable binary/' > /etc/apt/sources.list.d/jenkins.list

sudo apt-get update 

sudo apt-get install jenkins -y
