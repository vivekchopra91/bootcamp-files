#!/bin/bash 

aws ec2 run-instances \
--image-id "ami-04505e74c0741db8d" \
--instance-type "t2.micro" \
--count 1 \
--subnet-id "subnet-036d81e3d88d4fa7b" \
--security-group-ids "sg-012fa4f5511a5b66f" \
--tag-specifications 'ResourceType=instance,Tags=[{Key=Name,Value=Jenkins},{Key=Environment,Value=Development},{Key=ProjectName,Value=SoftoBizDevOps},{Key=ProjectID,Value=20220110},{Key=EmailID,Value=devops@softobiz.com},{Key=MobileNo,Value=+919908823070}]' \
--key-name "2nd-key" \
--user-data file://install-jenkins-linux.txt --profile vd
